---
marp: true
---

# Kotlin Collection

---

# Kotlin Documentation Source
https://kotlinlang.org/docs/collections-overview.html

---

# Collection Types
- Mutable
A read-only interface that provides operations for accessing collection elements.
- Immutable
A mutable interface that extends the corresponding read-only interface with write operations: adding, removing, and updating its elements.

---

# List

---

# Set

---

# Pair

---

# Map

---