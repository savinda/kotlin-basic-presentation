---
marp: true
---

# Kotlin Basic

#### Savino Rifky
###### @savinda

---

# Kenapa Kotlin ketimbang Java?

- Interoperability dengan Java
- Bisa Functional Programing (Higher order function & lambda) & OOP
- Code nya biasanya lebih ringkas
- Lebih banyak fitur fitur di Kotlin yang tidak dimiliki Java
    - Null Safety
    - Extension Function
    - dll

---

# Basic Syntax
Java vs Kotlin

---

# Program Entrypoint
Java
```java
public class HelloWorld {
    public static void main(String[] args) {
        System.out.println("Hello, World!");
    }
}
```
Kotlin
```kotlin
fun main() {
    println("Hello, World!")
}
```

---

# Print to the standard output
Java
```java
System.out.print("Hi!");
System.out.println("Hello, World!");
```
Kotlin
```kotlin
print("Hi!")
println("Hello, World!")
```

---

# Variables
Java
```java
Integer a = 10;
// Since Java 10
var x = 100;
```
Kotlin
```kotlin
val a: Int = 10
val b = 10
val c: Int
c = 3
```

---

# val vs var
Read-only local variables are defined using the keyword `val`.
They can be assigned a value only once.
```kotlin
val a: Int = 1  // immediate assignment
val b = 2   // `Int` type is inferred
val c: Int  // Type required when no initializer is provided
c = 3       // deferred assignment
```

---

# val vs var
Variables that can be reassigned use the `var` keyword.
```kotlin
var x = 5 // `Int` type is inferred
x += 1
```

---

# Quiz #Variable
```kotlin
class Mahasiswa {
    var name: String = "Nama Mahasiswa"
}

fun main() {
    val mahasiswa = Mahasiswa()
    mahasiswa.name = "Savino"

    println(mahasiswa.name)
}
```

---

# Nullable values
A reference must be explicitly marked as nullable when null value is possible.
Nullable type names have `?` at the end.
```kotlin
fun main() {
    var greeting: String? = "Hello"
    greeting = null
    println(greeting?.length)
}
```

---

# Array
Java
```java
String[] fruits = new String[]{"Apple", "Orange", "Grape"}
```
Kotlin
```kotlin
val fruits: Array<String> = arrayOf("Apple", "Orange", "Grape")
// val fruits = arrayOf("Apple", "Orange", "Grape")
```

---

# Array
```kotlin
val fruits = arrayOf("Apple", "Orange", "Grape")
println(fruits.size) // 3
println(fruits[0]) // Apple
println(fruits.get(0)) // Apple

fruits[1] = "Banana"
println(fruits[1]) // Banana
```

---

# Nullable Array
```kotlin
val fruits = arrayOfNulls(3)
println(fruits.size)
fruits[0] = null
fruits[1] = "Banana"
println(fruits)
```

---

# Range
```kotlin
val satuSampaiSeratus = 1..100
val seratusSampaiSatu = 100 downTo 1

val bilanganGenap = 10..100 step 2
println(bilanganGenap.count()) // 46
println(bilanganGenap.first) // 10
println(bilanganGenap.last) // 100
println(bilanganGenap.step) // 2
```

---

# String template
```kotlin
fun main() {
    val name = "Savino"
    val greet = "Hello, $savino"
    println(greet)
}
```

---

# Quiz #StringTemplate
```kotlin
fun main() {
        val name = "Savino"
    println("$name.length is ${name.length}")
}
```

---

# Control Flow

---

# If expression
```kotlin
var max = a
if (a < b) max = b

// With else
var max: Int
if (a > b) {
    max = a
} else {
    max = b
}

```

---

# Ternary Operator
Kotlin has no ternary operator `condition ? then : else`
```kotlin
// As expression
val max = if (a > b) a else b
```

---

# When expression
Similiar to the `switch case` statement in Java
```kotlin
when (x) {
    1 -> print("x == 1")
    2 -> print("x == 2")
    else -> {
        print("x is neither 1 nor 2")
    }
}
```
---

# Loops

---

# For loops
Java
```java
for(int i = 1; i <= 10; i++) {  
    System.out.println(i);  
}
```
Kotlin
```kotlin
for (i in 1..10) {
    println(i)
}
for (i in 6 downTo 0 step 2) {
    println(i)
}
// Iterate through collection in single expression
for (item in collection) {
    // ...
}
```

---

# While loops
```kotlin
while (x > 0) {
    x--
}
```

---

# Do While loops
```kotlin
do {
    val y = retrieveData()
} while (y != null)
```

---

# Break & Continue
- `return` by default returns from the nearest enclosing function or anonymous function.
- `break` terminates the nearest enclosing loop.
- `continue` proceeds to the next step of the nearest enclosing loop.

---

# Break
```kotlin
fun main() {
    var i = 0
    while (i < 10) {
        i++
        println(i)
        if (i >= 5) {
            break
        }
    }
}
```

---

# Quiz #Break
```kotlin
fun main() {
    for (i in 1..10) {
        for (j in 1..10) {
            if (j > 5) {
                break
            }
            println("$i $j")
        }
    }
}
```

---

# Continue
```kotlin
fun main() {
    for (i in 1..100) {
        if (i % 5 == 0) {
            println("Kelipatan 5: $i")
            continue
        }
        println(i)
    }
}
```

---

# Labels
Any expression in Kotlin may be marked with a label. Labels have the form of an identifier followed by the `@` sign, such as `abc@` or ` fooBar@`. To label an expression, just add a label in front of it.
```kotlin
fun main() {
    loop1@ for (i in 1..10) {
        for (j in 1..10) {
            if (j > 5) {
                break@loop1
            }
            println("$i $j")
        }
    }
}
```

---

# Latihan #FizzBuzz
Buatlah sebuah program console yang mencetak baris demi baris dari angka 1 sampai N, dimana N dapat di input oleh user.
Namun, untuk angka yang merupakan kelipatan tiga, cetak "Fizz", dan untuk angka yang merupakan kelipatan lima, cetak "Buzz". Apabila angka merupakan kelipatan tiga dan lima, cetak "FizzBuzz".

https://gitlab.com/savinda/best-kotlin-assesment/-/tree/soal/fizz-buzz

---

# All About Functions

---

# Functions
```kotlin
fun main() {
    greet()
}

fun greet() {
    println("Hello Budi")
}
```

---

# Functions Arguments
```kotlin
fun main() {
    greet("Budi")
    greet("Budi", "Setiawan")
}

fun greet(name: String) {
    println("Hello $name")
}

fun greet(firstName: String, lastName: String) {
    println("Hello $firstName $lastName")
}
```

---

# Function Default Arguments
```kotlin
fun main() {
    greet("Budi")
    greet("Budi", "Setiawan")
}

fun greet(firstName: String, lastName: String = "") {
    println("Hello $firstName $lastName")
}
```

---

# Function Named Arguments

```kotlin
fun main() {
    greet(lastName = "Setiawan")
    greet(firstName = "Budi", lastName = "Setiawan")
}

fun greet(firstName: String = "", lastName: String) {
    println("Hello $firstName $lastName")
}
```

---

# Unit-returning functions
```kotlin
fun main() {
    greet()
}

fun greet(): Unit {
    println("Hello Budi")
}
```
---

# Function return type
```kotlin
fun main() {
    println(add(10, 12))
}

fun add(a: Int, b: Int): Int {
    val total = a + b
    return total
}
```

---

# Single Expression Function
```kotlin
fun main() {
    println(add(10, 12))
    greet()
}

fun add(a: Int, b: Int) = a + b
fun greet() = println("Hello Budi")
```

---

# Variable number of arguments (varargs)
```kotlin
fun main() {
    val total = sum(1, 2, 3, 4, 5)
    println(total)
}

fun sum(varargs values: Int): Int {
    var total = 0
    for (value in values) {
        total += value
    }
    return value
}
```

---

# Extension Function
```kotlin
fun String.greet(): String {
    return "Hi, $this"
}

fun main() {
    val name = "savino"
    println(name.greet())
}
```

---

# Latihan #AngkaRomawi

Buatlah sebuah program yang menerima angka Romawi dari user dan ubah angka dalam angka desimal.
```
Symbol       Value
I             1
V             5
X             10
L             50
C             100
D             500
M             1000
```

---

# Latihan #AngkaRomawi

Sample:
```
Input: V
Output: 5

Input: IV
Output: 4

Input: LVIII
Output: 58
Explanation: L = 50, V= 5, III = 3.
```

Syarat:
input hanya boleh angka romawi yang valid, misal 4 bukan `IIII` tapi `IV`

https://gitlab.com/savinda/best-kotlin-assesment/-/tree/soal/angka-romawi

---

# Infix Notation
Infix functions must meet the following requirements:
- They must be member functions or extension functions.
- They must have a single parameter.
- The parameter must not accept variable number of arguments (varargs) and must have no default value.

---

# Infix Notation Example
```kotlin
infix fun Int.tambah(x: Int): Int {
    return this + x
}

fun main() {
    println(5 tambah 5)
}
```

---

# Function Scope
##### Local Function
```kotlin
fun main() {
    fun greet() {
        println("hello world")
    }

    greet()
}
```

---

# Return If
```kotlin
fun evaluateNumber(number: Int): String {
    return if (number % 2 == 0) {
        "Bilangan genap"
    } else {
        "Bilangan ganjil"
    }
}
```

---

# Return When
```kotlin
fun evaluateNumber(number: Int): String {
    return when {
        number % 2 == 0 -> "Bilangan genap"
        else -> "Bilangan ganjil"
    }
}
```

---

# Recursive Function
#### for loops
```kotlin
fun fibbonaciNumber(n: Int): Int {
    if (n < 2) {
        return n
    }
    var n0 = 0
    var n1 = 1
    for (n in 2..n) {
        val sum = n0 + n1
        n0 = n1
        n1 = sum
    }
    return n1
}
```

---

# Recursive Function
#### recursive
```kotlin
fun fibbonaciNumber(n: Int): Int {
    if (n < 2) {
        return n
    }
    return fibbonaciNumber(n - 1) + fibbonaciNumber(n - 2)
}
```

---

# Tail Recursive Function
```kotlin
tailrec fun fibonacci(n: Int, a: Int = 0, b: Int = 1): Int =
    when (n) {
        0 -> a
        1 -> b
        else -> fibonacci(n - 1, b, a + b)
    }
```

---

# Lambda Expression
```kotlin
val sum: (Int, Int) -> Int = { x: Int, y: Int -> x + y }
val sum = { x: Int, y: Int -> x + y }
```
- A lambda expression is always surrounded by curly braces.

- Parameter declarations in the full syntactic form go inside curly braces and have optional type annotations.

- The body goes after the ->.

- If the inferred return type of the lambda is not Unit, the last (or possibly single) expression inside the lambda body is treated as the return value.

---

# Higher-order Functions
A function that takes functions as parameters, or returns a function.
```kotlin
fun nameFormatter(name: String, action: (String) -> String): String {
    return "User Full Name: ${action(name)}"
}
fun main() {
    val toUpper = { name: String -> name.uppercase() }
    val toLower = { name: String -> name.lowercase() }
    println(nameFormatter("Savino", toUpper))
    println(nameFormatter("Savino", toLower))
}
```

---

# Higher-order Functions
Trailing lambda
```kotlin
fun nameFormatter(name: String, action: (String) -> String): String {
    return "User Full Name: ${action(name)}"
}
fun main() {
    val name = nameFormatter("Savino") { name: String -> name.uppercase() }
    println(name)
}
```

---

# Anonymous Functions
```kotlin
fun nameFormatter(name: String): String {
    return "User Full Name: ${action(name)}"
}
fun main() {
    val nameFormatter = fun(name: String): String {
        return "User Full Name: ${action(name)}"
    }
    println(nameFormatter("Savino"))
}
```

---

# Closure
```kotlin
fun main() {
    var count = 0
    val add = fun(i: Int) {
        count += i // accessing variable out of function outer scope
    }

    println(count)
    add(10)
    println(count)
}
```

---

# What Next?
- Kotlin OOP
- Kotlin Collection
- Kotlin Generic
- Kotlin Unit Test