---
marp: true
---
# Savino

---

# Class
```kotlin
class User
```

---

# Object
```kotlin
class User

val user = User()
```

---

# Properties
```kotlin
class User {
    val name: String
    var age: Int
}
```

---

# Constructor
```kotlin
class User(nameArg: String, ageArg: Int) {
    val name: String = nameArg
    var age: Int = ageArg
}
```

---
# Init Block
```kotlin
class User {
    val name: String
    var age: Int

    init {
        println("User Object has been initialized")
    }
}
```
---
# Secondary Constructor
```kotlin
class User(nameArg: String) {
    val name: String = nameArg
    var age: Int = 0
    constructor(nameArg: String, ageArg: Int): this(nameArg) {
        age = ageArg
    }
}

fun main() {
    val user1 = User("panjul")
    println(user1.name)
    println(user1.age)
    
    val user2 = User("panjul", 17)
    println(user2.name)
    println(user2.age)
}
```
---
# Properties in Constructor
```kotlin
class User(val name: String, val age: Int) {
}

fun main() {
    val user = User("panjul", 17)
    println(user.name)
    println(user.age)
}
```
---
# Function
```kotlin
class User(val name: String) {
    fun greet() {
        println("Hello, $name")
    }
}

fun main() {
    val user = User("panjul")
    user.greet()
}
```
---
# Inheritance
By default, Kotlin classes are `final` – they can’t be inherited.
To make a class inheritable, mark it with the `open` keyword
```kotlin
open class User(val username: String) {
    fun greet() {
        println("Hello, $username")
    }
}

final class PremiumUser(username: String): User(username)
final class FreeUser(username: String): User(username)

fun main() {
    val premiumUser = PremiumUser("panjul")
    premiumUser.greet()
    
    val freeUser = FreeUser("budi")
    freeUser.greet()
}
```
---
# Function Overriding
```kotlin
open class User(val username: String) {
    open fun greet() {
        println("Hello, $username")
    }
}

final class PremiumUser(username: String): User(username) {
    override fun greet() {
        println("Hello, Premium User $username")
    }
}
final class FreeUser(username: String): User(username)

fun main() {
    val premiumUser = PremiumUser("panjul")
    premiumUser.greet()
    
    val freeUser = FreeUser("budi")
    freeUser.greet()
}
```
---
# Properties Overriding
```kotlin
open class User {
    open val username: String = ""
}

open class PremiumUser: User() {
    override val username: String = "premium"
}
```
---
# Super
```kotlin
open class User {
    open val username: String = "u-"

    open fun greet() {
        println("Welcome, User")
    }
}

open class PremiumUser: User() {
    // Super Properties
    override val username: String = "${super.username}premium"
    // Super function
    override fun greet() {
        super.greet()
        println("You logged in as Premium User")
    }
}
```
---
# Abstract Class
```kotlin
abstract class Payment
class CashInHandPayment: Payment()
```
---

# Getter & Setter
Java
```java
public class Lender {
    private String email;
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email
    }
}
```
---
# Getter & Setter
Kotlin Syntax
```kotlin
var <propertyName>[: <PropertyType>] [= <property_initializer>]
    [<getter>]
    [<setter>]
```
---
# Getter & Setter
```kotlin
class Lender {
    var email: String = "lender@investree.id"
        get() = field
        set(value) {
            field = value
        }
}
```
---

# Late Init
```kotlin
class Lender {
    lateinit var name: String

    fun setDefaultName() {
        name = "Lender"
    }
}

main() {
    val lender = Lender()
    lender.setDefaultName()
    println(lender.name)
}
```
---

# Interface
```kotlin
interface Fly {
    fun fly()
}
interface Walk {
    fun walk()
}
interface Swim {
    fun swim()
}
class Duck(): Walk
class Bird(): Walk, Fly
class Dolphin(): Swim
```
---

# Visibility
- `private` means that the member is visible inside this class only (including all its members).

- `protected` means that the member has the same visibility as one marked as private, but that it is also visible in subclasses.

- `internal` means that any client inside this module who sees the declaring class sees its internal members.

- `public` means that any client who sees the declaring class sees its public members.

---

# toString
Returns a string representation of the object
```kotlin
class User(val name: String) {
    override fun toString(): String {
        return "User[name: $name]"
    }
}
```
---

# equals
Structural equality (`==` - a check for `equals()`)
```kotlin
class User(val name: String)
fun main() {
    val lender = User("Savino")
    val borrower = User("Savino")
    println(lender == borrower)
}
```

---

# equals
```kotlin
class User(val name: String) {
    override fun equals(obj: Any?): Boolean {
        return if (obj is User) {
            name == obj.name
        } else {
            false
        }
    }
}
```

---

# hashCode
Returns a hash code value for the object or zero if the object is `null`
```kotlin
class User(val name: String)
fun main() {
    val lender = User("Savino")
    val borrower = User("Savino")
    println(lender.hashCode() == borrower.hashCode())
}
```

---

# hashCode
Returns a hash code value for the object or zero if the object is `null`
```kotlin
class User(val name: String) {
    override fun hashCode(): Int {
        return name.hashCode()
    }
}
```

---

# Data Class
```kotlin
data class Lender(
    val name: String,
    val email: String,
    val cashInHand: Long = 0
)
fun main() {
    val lender1 = Lender(
        name = "Savino",
        email = "lender@investree.id",
        cashInHand = 1_000_000L
    )
    val lender2 = Lender(
        name = "Savino",
        email = "lender@investree.id",
        cashInHand = 1_000_000L
    )
    println(lender1 == lender2)
}
```
---

# Inner Class
```kotlin
class User(val name: String) {
    inner class Lender(val name: String) {
        val username = "lender@$name"
    }
}
fun main() {
    val user = User("Savino")

    val lender = user.Lender("Rifky")
    println(lender.username)
}
```
---

# Enum Class
```kotlin
enum class Direction {
    NORTH, SOUTH, WEST, EAST
}
enum class Gender {
    MALE, FEMALE
}
enum class UserRole {
    LENDER, BORROWER
}
```
---

# Enum Class Properties & Function
```kotlin
enum class Direction(val alias: Char) {
    NORTH('N'), SOUTH('S'), WEST('W'), EAST('E');

    fun toAlias(): Char {
        return alias
    }
}

fun main() {
    println(Direction.NORTH)
    println(Direction.NORTH.toAlias())
}
```

---

# Exception
```kotlin
class ValidationException: Throwable()
data class User(name: String? = null)

fun main() {
    val user = User()
    validateUser(user)
}

fun validateUser(user: User) {
    if (user.name == null) {
        throw ValidationException()
    }
}
```

---

# Try Catch

```kotlin
class ValidationException: Throwable()
data class User(name: String? = null)

fun main() {
    val user = User()
    try {
         validateUser(user)
    } catch(ex: ValidationException) {
        println(ex.message)
    }
}

fun validateUser(user: User) {
    if (user.name == null) {
        throw ValidationException()
    }
}
```

---

# Finally

```kotlin
class ValidationException: Throwable()
data class User(name: String? = null)

fun main() {
    val user = User()
    try {
         validateUser(user)
    } catch(ex: ValidationException) {
        println(ex.message)
    } finally {
        // Closing Resources/sending log
        println("Finally has been executed")
    }
}

fun validateUser(user: User) {
    if (user.name == null) {
        throw ValidationException()
    }
}
```

---

# Scope Function
- `let`
- `run`
- `with`
- `apply`
- `also`

---

# Scope Function - Let
```kotlin
fun printNumber(number: Int?) {
    number?.let {
        println(it)
    }
}

fun main() {
    var number: Int? = 10
    printNumber(number)

    number = null
    printNumber(number)
}
```

---

# Scope Function - Run
```kotlin
fun printNumber(number: Int?) {
    number?.run {
        println(this)
    }
}

fun main() {
    var number: Int? = 10
    printNumber(number)

    number = null
    printNumber(number)
}
```

---

# Scope Function - With
```kotlin
data class User(val name: String)

fun main() {
    val user = User("savino")
    with(user) {
        println(this.user)
    }
}
```

---

# Scope Function - Apply
```kotlin
data class User(var name: String)

fun main() {
    val user = User("savino")
    user.apply {
        name = "rifky"
    }

    println(user)
}
```

---

# Scope Function - Also
```kotlin
data class User(
    var name: String,
    var phoneNumber: String? = null
)

fun main() {
    val user = User("savino").also {
        it.phoneNumber = "081234"
    }

    println(user)
}
```

---

# What's next?
- Kotlin Collection
- Kotlin Generic
- Kotlin Unit Test
- Spring Boot
